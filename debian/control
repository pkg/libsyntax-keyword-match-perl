Source: libsyntax-keyword-match-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libextutils-cbuilder-perl,
               libfuture-asyncawait-perl <!nocheck>,
               libfuture-perl <!nocheck>,
               libmodule-build-perl,
               libtest-simple-perl <!nocheck>,
               libxs-parse-keyword-perl,
               perl-xs-dev,
               perl:native
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libsyntax-keyword-match-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libsyntax-keyword-match-perl.git
Homepage: https://metacpan.org/release/Syntax-Keyword-Match
Rules-Requires-Root: no

Package: libsyntax-keyword-match-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends},
         libxs-parse-keyword-perl
Description: match/case syntax plugin for perl
 Syntax::Keyword::Match provides a syntax plugin that implements a
 control-flow block called match/case, which executes at most one of a choice
 of different blocks depending on the value of its controlling expression.
 .
 This is similar to C's switch/case syntax (copied into many other languages),
 or syntax provided by Switch::Plain.
 .
 This is an initial, experimental implementation. Furthermore, it is built as
 a non-trivial example use-case on top of XS::Parse::Keyword, which is also
 experimental. No API or compatibility guarantees are made at this time.
